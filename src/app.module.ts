import { Module } from '@nestjs/common';
import { BeersModule } from './beers/beers.module';

@Module({
  imports: [BeersModule],
})
export class ApplicationModule {}
