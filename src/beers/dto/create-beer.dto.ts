export class CreateBeerDto { // Data Transfer Object
  readonly name: string;
  readonly price: number;
  readonly description: string;
}
