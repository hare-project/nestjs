export class UpdateBeerDto { // Data Transfer Object
  readonly name: string;
  readonly price: number;
  readonly description: string;
}
