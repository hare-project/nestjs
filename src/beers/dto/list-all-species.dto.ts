export class ListAllSpecies { // Data Transfer Object
  readonly name: string;
  readonly price: number;
  readonly description: string;
}
