import * as mongoose from 'mongoose';

export const BeerSchema = new mongoose.Schema({
  name: String,
  price: Number,
  description: String,
});
