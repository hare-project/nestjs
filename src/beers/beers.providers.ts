import { Connection } from 'mongoose';
import { BeerSchema } from './schemas/beer.schema';

export const beersProviders = [
  {
    provide: 'CAT_MODEL',
    useFactory: (connection: Connection) => connection.model('Beer', BeerSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
