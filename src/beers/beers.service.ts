import { Model } from 'mongoose';
import { Injectable} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Beer } from './interfaces/beer.interface';
import { CreateBeerDto } from './dto/create-beer.dto';

@Injectable()
export class BeersService {
  constructor(@InjectModel('Beer') private readonly beerModel: Model<Beer>) {}
  // private readonly beers: Beer[] = [];

    async create(createBeerDto: CreateBeerDto): Promise<Beer> {
    const createdBeer = new this.beerModel(createBeerDto);
    return await createdBeer.save();
    }
    // create(beer: Beer) {
    // this.beers.push(beer);
    // }
    // findAll(): Beer[] {
    // return this.beers;
    async findAll(): Promise<Beer[]> {
      return await this.beerModel.find().exec();
  }
}
