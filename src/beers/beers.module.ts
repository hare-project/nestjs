import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BeersController } from './beers.controller';
import { BeersService } from './beers.service';
import { BeerSchema } from './schemas/beer.schema';
import { DatabaseModule } from '../database/database.module';
import { beersProviders } from './beers.providers';

@Module({
  imports: [DatabaseModule], // MongooseModule.forFeature([{ name: 'Beer', schema: 'BeerSchema' }])],
  controllers: [BeersController],
  providers: [BeersService, ...beersProviders],
  //   {
  //     provide: getModelToken('Beer'), // from @nestjs/mongoose
  //     useValue: beerModel,
  //   },
  // ],
})
export class BeersModule {}
