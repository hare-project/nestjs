import { Document } from 'mongoose';

export interface Beer extends Document {
  name: string;
  price: number;
  description: string;
}
