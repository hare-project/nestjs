import { Body, Controller, Put, Delete, Get, Post } from '@nestjs/common';
import { Param } from '@nestjs/common/decorators/http/route-params.decorator';
import {BeersService} from './beers.service';
import {Beer} from './interfaces/beer.interface';
import { CreateBeerDto } from './dto/create-beer.dto';
import { UpdateBeerDto } from './dto/update-beer.dto';
import { ListAllSpecies} from './dto/list-all-species.dto';

@Controller('beers')
export class BeersController {
  constructor(private readonly beerService: BeersService) {}

  @Post()
  async create(@Body() createBeerDto: CreateBeerDto) { // @Req -- decorator
    this.beerService.create(createBeerDto);
    return 'This action adds a new beer';
  }
  @Get()
  async findAll(): Promise<Beer[]> { // (@Query()query: ListAllSpecies) {
    return this.beerService.findAll(); // return 'This action returns all beers(limit: ${query.limit} items)'; // czemu query limit nie podkreśla?
  }
  @Get(':id')
  findOne(@Param('id') id): string {
    return `This action returns a #${id} beer`;
  }
  @Put(':id')
  update(@Param('id') id: string, @Body() updateBeerDto: UpdateBeerDto) {
    return 'This action updates a #${id} beer';
  }
  @Delete(':id')
  remove(@Param('id') id: string) {
    return 'This action removes a #${id} beer';
  }
}
// można użyć expressa
